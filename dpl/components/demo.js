/**
 * @file SQ.demo
 * @data 2013.5.24
 * @version 1.0.0
 */

/*global
 window: false,
 document: false,
 setTimeout : false,
 setInterval: false,
 clearInterval: false,
 history: false,
 $: false,
 SQ: true,
 slip: false,
 Zepto: true
 */

/**
 * @chagelog
 * 1.0.0  + 新增对话框组件
 */

/**
 * @todolist
 * 
 */

//var SQ = SQ || {};
SQ.lib = Zepto;

(function ($, window) {
    /**
     * @name Demo
     * @classdesc 对话框组件，依赖 jQuery 或 Zepto 库。
     * @constructor
     * @param {string} config.TXT_UNKNOWNERROR 通过 Ajax 接收到的数据无法识别
     * @param {string} config.TXT_UNKNOWNERROR 通过 Ajax 接收到的数据无法识别
     * @example var appList = new SQ.Demo({
            EVE_EVENTTYPE : "scroll",
            DOM_TRIGGERTARGET : window,
            DOM_AJAXBOX : ".J_ajaxWrap",
            DOM_STATEBOX : ".J_scrollDemo"
        });
     * @requires jQuery or Zepto
     * @version 1.0.0
     */
    function Demo(config) {
        var me = this;
        var i;

        me.config = {

        };

        for (i in config) {
            if (config.hasOwnProperty(i)) {
                me.config[i] = config[i];
            }
        }

        me.$triggerTarget = $(me.config.DOM_TRIGGERTARGET); // 触发元素

        me.showFun = me.config.open;

        if (me._verify()) {
            me._init();
        }
    }
    Demo.prototype =  {
        construtor: Demo,
        version: "1.0.0",

        /** 验证参数是否合法 */
        _verify : function () {
            return true;
        },

        /** 验证参数是否合法 */
        _init : function (e) {
            var me = this;
        },

        /**
         * 事件绑定方法。
         * @param {object} $el jQuert 或 Zepto 元素包装集。
         * @param {string} EVE_EVENTTYPE 事件类型，"scroll" 或 "click"。
         */
        _bind : function ($el, EVE_EVENTTYPE) {
            var me = this;
            $el.bind(EVE_EVENTTYPE, function () {
                me._trigger(EVE_EVENTTYPE);
            });
        },
        /**
         * 解除事件绑定方法。
         * @param {object} $el jQuert 或 Zepto 元素包装集。
         * @param {string} EVE_EVENTTYPE 事件类型，"scroll" 或 "click"。
         */
        _unBind : function ($el, EVE_EVENTTYPE) {
            $el.unbind(EVE_EVENTTYPE);
        },
        /**
         * 触发事件方法，在满足绑定事件条件时或满足指定触发条件的情况下调用触发方法，
         * 该方法用于集中处理触发事件，判定是否需要加载数据或者更新 UI 显示。
         * @param {string} EVE_EVENTTYPE 事件类型，"scroll" 或 "click"。
         */
        _trigger : function (EVE_EVENTTYPE) {
            var me = this;
        },

        // 重置位置与尺寸
        _reset : function () {
            var me = this;
        },

        /**
         * 尺寸
         * @param {Number, String} 宽度
         * @param {Number, String} 高度
         */
        size : function (width, height) {
            var me = this;
        },

        /** 显示对话框 */
        show : function () {
            var me = this;
            // 执行回调函数
            me.showFun && me.showFun(e);
        },

        /** 关闭对话框 */
        close : function () {
            var me = this;
        },

        /** 隐藏对话框 */
        hide : function () {
            var me = this;
        },

        /**
         * 定时关闭
         * @param {Number} 单位为秒, 无参数则停止计时器
         */
        time : function (second) {
            var me = this;
        }
    };
    SQ.Demo = Demo;
}(SQ.lib, window));